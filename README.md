                                                           
                              Conception-Programmation Web / Serveur


          
          Application « Racoin.net »

Description générale

L'application « Racoin.net » est une application en ligne pour la publication de petites annonces.

L'application permet de mettre en lignes des annonces, rechercher et visualiser des annonces, contacter l'annonceur d'une annonce, gérer les annonces mises en ligne (suppression, modification). Les annonces sont classées par département et par catégorie (Immobilier/locations, Immobilier/vente, véhicule/auto, véhicule/moto ... une seule catégorie par annonce). Les
annonceurs sont identifiés par leur adresse e-mail.

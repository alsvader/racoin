<?php

class SignupView extends AbstractView {

	public function __construct($menu, $categories, $url_post, $token, $errors = null){

		$this->layout = "signup.twig";
		$this->addVar("menu", $menu);
		$this->addVar("categories", $categories);
		$this->addVar("url_post", $url_post);
		$this->addVar("token", $token);
		$this->addVar("errors", $errors);

	}
}

?>
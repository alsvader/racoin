<?php

class ErreurView extends AbstractView {

	public function __construct($id, $menu){ 

		$this->layout = "erreur.twig";
		$this->addVar('id', $id);
		$this->addVar('menu', $menu);
	}
}

?>
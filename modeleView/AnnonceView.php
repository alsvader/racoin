<?php

class AnnonceView extends AbstractView {
    	
    	public function __construct(){ 
	
		}
        
        public function all_annonces($menu, $annonces, $uri, $logged = false, $mail = null) {
            
            $this->layout = "annonces.twig";
            $this->addVar("menu", $menu);
            $this->addVar("uri", $uri);
            $this->addVar("annonces", $annonces);
            $this->addVar("logged", $logged);
            $this->addVar("mail", $mail);

        }

        public function annonceById($annonce, $menu, $uri, $update, $logged = false, $mail = null){

            $this->layout = "annonceId.twig";
            $this->addVar('annonce', $annonce);
            $this->addVar('menu', $menu);
            $this->addVar('uri', $uri);
            $this->addVar('update', $update);
            $this->addVar('logged', $logged);
            $this->addVar('mail', $mail);

        }

        public function updateAnnonce($menu, $annonce, $put, $categories, $annuler, $token){

            $this->layout = "update.twig";
            $this->addVar('menu', $menu);
            $this->addVar('annonce', $annonce);
            $this->addVar('put', $put);
            $this->addVar('categories', $categories);
            $this->addVar('annuler', $annuler);
            $this->addVar('token', $token);
        }

        public function search($categ, $menu, $categories, $detaille, $search, $logged = false, $mail = null, $annoncesByCategorie = null){

            $this->layout = "annoncesByCategorie.twig";
            $this->addVar('cat', $categ);
            $this->addVar('menu', $menu);
            $this->addVar('annoncesByCategorie', $annoncesByCategorie);
            $this->addVar('categories', $categories);
            $this->addVar('detaille', $detaille);
            $this->addVar('search', $search);
            $this->addVar('logged', $logged);
            $this->addVar('mail', $mail);

        }
}

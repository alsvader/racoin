<?php

class AccueilView extends AbstractView {

	public function __construct($menu, $list_annonce, $top, $uri, $logged = false, $mail = null){ 

		$this->layout = "accueil.twig";
		$this->addVar('menu', $menu);
		$this->addVar('list_annonce', $list_annonce);
		$this->addVar('top', $top);
		$this->addVar('uri', $uri);
		$this->addVar('logged', $logged);
		$this->addVar('mail', $mail);

	}


}

?>
<?php 
	
	class FormView extends AbstractView{

		public function MontrerForm ($menu, $categorie, $insert, $leTok){
			$this->layout = "FormInsert.twig";
			$this->addVar("menu", $menu);
			$this->addVar("categorie", $categorie);
			$this->addVar("insert", $insert);
			$this->addVar("leTok", $leTok);
		}
	} 

?>
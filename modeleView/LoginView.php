<?php


class LoginView extends AbstractView {
    
    public function login($menu, $uri) {
        $this->layout = "login.twig";
        $this->addVar("menu", $menu);
        $this->addVar("uri", $uri);
    }

    public function __construct($menu, $uri, $token, $error = false){

    	$this->layout = "login.twig";
    	$this->addVar("menu", $menu);
    	$this->addVar("uri", $uri);
    	$this->addVar("token", $token);
    	$this->addVar("error", $error);
    }
}

-- phpMyAdmin SQL Dump
-- version 3.4.11.1deb2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Dec 11, 2013 at 04:48 AM
-- Server version: 5.5.31
-- PHP Version: 5.4.4-14+deb7u5

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `racoin`
--

-- --------------------------------------------------------

--
-- Table structure for table `annonces`
--

CREATE TABLE IF NOT EXISTS `annonces` (
  `id_annonce` int(11) NOT NULL AUTO_INCREMENT,
  `titre` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `descr` text COLLATE utf8_unicode_ci NOT NULL,
  `prix` double NOT NULL,
  `ville` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `cp` int(11) NOT NULL,
  `id_user` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `id_categorie` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id_annonce`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=7 ;

--
-- Dumping data for table `annonces`
--

INSERT INTO `annonces` (`id_annonce`, `titre`, `descr`, `prix`, `ville`, `cp`, `id_user`, `id_categorie`, `created_at`, `updated_at`) VALUES
(1, 'annonce', 'New annonce ', 0, 'Laxou', 25000, 'daniel_serna18@hotmail.com', 1, '2013-12-10 00:00:00', '0000-00-00 00:00:00'),
(2, 'WoW', 'World of Warcraft - Blizzard', 1.2, 'Nancy', 0, 'yayo.1@yahoo.fr', 0, '2013-12-11 00:00:00', '0000-00-00 00:00:00'),
(3, 'StarCraft', 'Wings of Liberty - Blizzard', 200.99, 'Paris', 0, 'bnet@battle.net', 0, '2013-12-11 00:00:00', '0000-00-00 00:00:00'),
(4, 'Diablo', 'Reaper of souls - Blizzard', 100.99, 'Paris', 0, 'diablo@battle.net', 0, '2013-12-09 00:00:00', '0000-00-00 00:00:00'),
(5, 'Diablo', 'Reaper of souls - Blizzard', 100.99, 'Paris', 0, 'diablo@battle.net', 0, '2013-12-11 03:54:40', '2013-12-11 03:54:40'),
(6, '', '', 299.99, '', 0, '', 0, '2013-12-11 03:57:42', '2013-12-11 03:57:42');

-- --------------------------------------------------------

--
-- Table structure for table `categorie`
--

CREATE TABLE IF NOT EXISTS `categorie` (
  `id_categorie` int(11) NOT NULL AUTO_INCREMENT,
  `categorie` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `descr_categorie` text COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id_categorie`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Dumping data for table `categorie`
--

INSERT INTO `categorie` (`id_categorie`, `categorie`, `descr_categorie`) VALUES
(1, 'games', 'All online bnet games');

-- --------------------------------------------------------

--
-- Table structure for table `photo`
--

CREATE TABLE IF NOT EXISTS `photo` (
  `id_photo` int(11) NOT NULL AUTO_INCREMENT,
  `source` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id_photo`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `email` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `nom` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `telephone` int(11) NOT NULL,
  PRIMARY KEY (`email`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

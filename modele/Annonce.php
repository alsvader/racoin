<?php

require 'vendor/autoload.php';

use Illuminate\Database\Eloquent\Model as Model;
use Illuminate\Database\Capsule\Manager as DB;

class Annonce extends Model {
    
    protected $table = "annonces";
    protected $primaryKey = "id_annonce";
    public $timestamps = true;

    public function getAnnoncesByDate() {
        return $this->select(array("titre", "descr", "prix", DB::raw("date(created_at) as date", "categorie")))->join("categorie", "categorie.id_categorie","=" ,"annonces.id_categorie")->orderBy("created_at", "desc")->get();

    }

    public function categorie(){

    	return $this->belongsTo('Categorie', 'id_categorie');
    }
    
    public function user() {
        return $this->belongsTo("User", "id_user", "email");
    }

    public function photo() {
        return $this->hasMany("Photo", "id_annonce");
    }
    
}

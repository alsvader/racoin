<?php

require 'vendor/autoload.php';

use Illuminate\Database\Eloquent\Model as Model;

class Photo extends Model
{
	protected $table = "photo";
    protected $primaryKey = "id_photo";
    public $timestamps = true;

    public function annonce()
    {
        return $this->belongsTo('annonces','id_annonce');
    }
}

?>
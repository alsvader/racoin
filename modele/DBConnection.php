<?php
//namespace modele;
require 'vendor/autoload.php';

use Illuminate\Database\Capsule\Manager as DB;

class DBConnection {

    public static function makeConection(){

            $conection=new DB;
            $conection->addConnection([
                'driver'=>'mysql',
                'host'=>'localhost',
                'database'=>'racoin',
                'username'=>'racoin',
                'password'=>'pdo',
                'charset'=>'utf8',
                'collation'=>'utf8_unicode_ci',
                'prefix'=>'',
            ]);

            $conection->setAsGlobal();
            $conection->bootEloquent();
    }
}

?>
<?php

//namespace modele;
require 'vendor/autoload.php';

use Illuminate\Database\Eloquent\Model as Model;

class Categorie extends Model{
   
    protected $table = "categorie";
    protected $primaryKey = "id_categorie";
    public $timestamps = false;
    
    public function annonces() {
        return $this->hasMany("Annonce", "id_categorie");
    }
}

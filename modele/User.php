<?php

require 'vendor/autoload.php';

use Illuminate\Database\Eloquent\Model as Model;

class User extends Model {
    
    protected $table = "user";
    protected $primaryKey = "email";
    public $timestamps = false;
    
    public function annonces() {
        return $this->hasMany("annonces", "id_user");
    }
    
}

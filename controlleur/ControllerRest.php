<?php

class ControllerRest{

	public function __construct(){}

	public function jsonAll($app){
	
		DBConnection::makeConection();
		$infos = Annonce::with("categorie")->select('titre as title', 'descr as description', 'prix as Tarif','ville as location','cp as code_Postal','id_user as email','created_at as date','id_categorie' )->get();
		return $infos->toJson();
	}

	
}

?>
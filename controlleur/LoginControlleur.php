<?php

/**
 * Description of LoginControlleur
 *
 * @author dan
 */
class LoginControlleur {
    static $listactions = array("login" => "do_login");
    
    public function do_login($menu, $uri) {
        
        $loginView = new LoginView();
        $loginView->login($menu, $uri);
        $loginView->display();
    }
}

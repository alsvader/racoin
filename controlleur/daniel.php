<?php


class AnnonceControlleur
{
	static $listactions = array('afficheAll'=>'afficheAll',
								'afficheById'=>'AfficheById');

	public function __construct()
	{}

	/*public function Default(array $request)
	{
		$this->DisplayAllCommand($request);
	}*/

	public function afficheAll($menu, $uri)
	{
		DBConnection::makeConection();
		$annonces = Annonce::with('categorie')->orderBy('created_at', 'DESC')->get();


		date_default_timezone_set('Europe/Paris');

		foreach ($annonces as $key) {
                    
                    $tab["id"] = $key->id_annonce;
                    $tab["titre"] = $key->titre;
                    $tab["descr"] = $key->descr;
                    $tab["prix"] = $key->prix;
                    $tab["photo_principal"] = $key->photo_principal;
                    $tab["categorie"] = $key->categorie->descr_categorie;
                    $tab["created_at"] = strtok($key->created_at, " ");
                    
                    $tab_annonce[] = $tab;
		}
                $view = new AnnonceView();
                $view->all_annoces($menu, $tab_annonce, $uri);
                $view->display();
	}

	public function afficheById($id, $menu, $uri){

		DBConnection::makeConection();
		$annonce = Annonce::with('user', 'categorie', 'photo')->where('id_annonce', '=', $id)->get();

		if (count($annonce) > 0) {
			
			$view = new AnnonceView();
			$view->annonceById($annonce, $menu, $uri);
			$view->display();

		}else {
			
			$view = new ErreurView($id, $menu);
			$view->display();
		}
	}

	public function DisplayAllCommand()
	{
		echo "erreur";
		var_dump($request);
	}

	public function accueil($menu, $uri){

		DBConnection::makeConection();

		$slide = Annonce::all();

		$top = Annonce::with('categorie', 'user')->orderBy('created_at', 'desc')->take(9)->get();
		
		$view = new AccueilView($menu, $slide, $top, $uri);
		$view->display();

	}

}


?>
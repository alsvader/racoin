<?php


class AnnonceControlleur
{
	static $listactions = array('afficheAll'=>'afficheAll',
								'afficheById'=>'AfficheById');

	public function __construct()
	{}


	public function afficheAll($menu, $uri)
	{
		DBConnection::makeConection();
		$annonces = Annonce::with('categorie')->orderBy('created_at', 'DESC')->get();


		date_default_timezone_set('Europe/Paris');

		foreach ($annonces as $key) {
                    
                    $tab["id"] = $key->id_annonce;
                    $tab["titre"] = $key->titre;
                    $tab["descr"] = $key->descr;
                    $tab["prix"] = $key->prix;
                    $tab["photo_principal"] = $key->photo_principal;
                    $tab["categorie"] = $key->categorie->descr_categorie;
                    $tab["created_at"] = strtok($key->created_at, " ");
                    
                    $tab_annonce[] = $tab;
		}
                $view = new AnnonceView();
                $view->all_annoces($menu, $tab_annonce, $uri);
                $view->display();
	}

	public function afficheById($id, $menu, $uri){

		DBConnection::makeConection();
		$annonce = Annonce::with('user', 'categorie', 'photo')->where('id_annonce', '=', $id)->get();

		if (count($annonce) > 0) {
			
			$view = new AnnonceView();
			$view->annonceById($annonce, $menu, $uri);
			$view->display();

		}else {
			
			$view = new ErreurView($id, $menu);
			$view->display();
		}
	}

	
	public function update($id, $menu, $put, $annuler){

		DBConnection::makeConection();
		$annonce = Annonce::with('categorie')->where('id_annonce', '=', $id)->get();
		$categories = Categorie::all();
		$view = new AnnonceView();

		$tok = $this->gererToken();

		$view->updateAnnonce($menu, $annonce, $put, $categories, $annuler, $tok);
		$view->display();
	}

	private function gererToken(){

		session_start();
		$token = hash("sha256", uniqid());
		$_SESSION['token'] = $token;

		return $token;
	}

	public function sauvegarder($id, $nomAnonce, $tarAnnonce, $villeAnnonce, $codePostal, $typeCat, $description) {

		$nomAnonce = strip_tags($nomAnonce);
		$villeAnnonce = strip_tags($villeAnnonce);
		$description = strip_tags($description);
		$tarAnnonce = strip_tags($tarAnnonce);
		$codePostal = strip_tags($codePostal);
		$id = strip_tags($id);
		$typeCat = strip_tags($typeCat);

		$nomAnonce = (empty($nomAnonce) or is_numeric($nomAnonce)) ? false : $nomAnonce;
		$villeAnnonce = (empty($villeAnnonce) or is_numeric($villeAnnonce)) ? false : $villeAnnonce;
		$description = (empty($description) or is_numeric($description)) ? false : $description;

		$tarAnnonce = (empty($tarAnnonce) or !is_numeric($tarAnnonce)) ? false : $tarAnnonce;
		$codePostal = (empty($codePostal) or !is_numeric($codePostal)) ? false : $codePostal;
		$id = (empty($id) or !is_numeric($id)) ? false : $id;
		$typeCat = (empty($typeCat) or !is_numeric($typeCat)) ? false : $typeCat; 

		if (!$nomAnonce or !$villeAnnonce or !$description or !$tarAnnonce or !$codePostal or !$id or !$typeCat) {
			
			$faite = false;

		}else {

			DBConnection::makeConection();
			date_default_timezone_set('Europe/Paris');

			$annonce = Annonce::find($id);

			$annonce->titre = $nomAnonce;
			$annonce->descr = $description;
			$annonce->prix = $tarAnnonce;
			$annonce->ville = $villeAnnonce;
			$annonce->cp = $codePostal;
			$annonce->id_categorie = $typeCat;
			$annonce->updated_at = date('Y-m-d H:i:s');

			$annonce->save();

			$faite = true;

		}	

		return $faite;
	}


	public function DisplayAllCommand()
	{
		echo "erreur";
		var_dump($request);
	}

	public function accueil($menu, $uri){

		DBConnection::makeConection();

		$slide = Annonce::all();

		$top = Annonce::with('categorie', 'user')->orderBy('created_at', 'desc')->take(9)->get();
		
		$view = new AccueilView($menu, $slide, $top, $uri);
		$view->display();

	}

}


?>
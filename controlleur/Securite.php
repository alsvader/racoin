<?php

use Egulias\EmailValidator\EmailValidator;


class Securite {

	public function __construct() {}
	
	public static function gererToken(){

		session_start();
		$token = hash("sha256", uniqid());
		$_SESSION['token'] = $token;

		return $token;
	}

	public static function validerMail($email){

		$validator = new EmailValidator;

		if($validator->isValid($email))
			return true;
		else
			return false;
	}

	public static function nameSession(){
		session_start();
		return $_SESSION['profil']['userid'];
	}

	public static function logout(){

		session_start();
		session_unset();
		session_destroy();

	}
}


?>
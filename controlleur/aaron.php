<?php

 
class ControlleurAaron
{
	static $listactions = array('afficheAll'=>'afficheAll',
								'afficheById'=>'AfficheById');

	public function __construct()
	{}


	public function afficheAll($menu, $uri, $logged = false, $mail = null)
	{
		DBConnection::makeConection();
		$annonces = Annonce::with('categorie')->orderBy('created_at', 'DESC')->get();


		date_default_timezone_set('Europe/Paris');

		foreach ($annonces as $key) {
                    
                    $tab["id"] = $key->id_annonce;
                    $tab["titre"] = $key->titre;
                    $tab["descr"] = $key->descr;
                    $tab["prix"] = $key->prix;
                    $tab["photo_principal"] = $key->photo_principal;
                    $tab["categorie"] = $key->categorie->descr_categorie;
                    $tab["created_at"] = strtok($key->created_at, " ");
                    
                    $tab_annonce[] = $tab;
		}
        
        $view = new AnnonceView();
        
        	if($logged){
        		$view->all_annonces($menu, $tab_annonce, $uri, $logged, $mail);
        	}else{
        		$view->all_annonces($menu, $tab_annonce, $uri);
        	}

        $view->display();
	}

	public function afficheById($id, $menu, $uri, $update, $logged = false, $mail = null){

		DBConnection::makeConection();
		$annonce = Annonce::with('user', 'categorie', 'photo')->where('id_annonce', '=', $id)->get();

		if (count($annonce) > 0) {
			
			$view = new AnnonceView();
			
			if($logged){
				$view->annonceById($annonce, $menu, $uri, $update, $logged, $mail);
			}else {
				$view->annonceById($annonce, $menu, $uri, $update);
			}
			$view->display();

		}else {
			
			$view = new ErreurView($id, $menu);
			$view->display();
		}
	}

	private function gererQuery($params){

		$query1 = "";
		$query2 = "";
		$query = "";

		if (count($params) == 0 ) {
			
			$query = null;
			
		}else {

			if (count($params) > 1) {
			
				foreach ($params as $key => $value) {
				
					$query1 .= ( $key == "tarif" ) ? "prix <= $value AND " : null;
					$query2 .= ( $key == "terme" ) ? " (titre LIKE '%$value%' OR descr LIKE '%$value%')" : null;
				}	

				$query = $query1 . " " . $query2;

			}else{

				foreach ($params as $key => $value) {
				
					$query1 .= ( $key == "tarif" ) ? "prix <= $value" : null;
					$query2 .= ( $key == "terme" ) ? " titre LIKE '%$value%' OR descr LIKE '%$value%'" : null;
				}

				$query = $query1 . " " . $query2;
			}
		}

		return $query;
	}

	public function search($menu, $detaille, $search, $logged = false, $mail = null, $categorie = null, $request = array()){

		$params = array();

			if (array_key_exists("tarif", $request) && is_numeric($request['tarif']) ) {
				
				$request['tarif'] = $request['tarif'];

			}else {
				$request['tarif'] = "";
			}


			if ( array_key_exists("typeCat", $request) ) {
				
				$request['typeCat'] = "";
			}

			foreach ($request as $key => $value) {

				if ( !(strcmp($value, "") == 0) ) {
					
					$params[$key] = strip_tags($value);
				}
				
			}

		$result = $this->gererQuery($params);

		DBConnection::makeConection();
		$view = new AnnonceView();

		if(!is_null($categorie)) {

			if (strcmp($categorie, "none") == 0) {

				if (is_null($result)) {
					
					$categories = Categorie::all(); 				

					if($logged){
						$view->search($categorie, $menu, $categories, $detaille, $search, $logged, $mail);
					}
					else{
						$view->search($categorie, $menu, $categories, $detaille, $search);
					}	

				}else {

					$categories = Categorie::all(); 
					$view = new AnnonceView();
					$annoncesByCategorie = Annonce::whereRaw($result)->get();

					if($logged){
						$view->search($categorie, $menu, $categories, $detaille, $search, $logged, $mail, $annoncesByCategorie);
						echo "lol";
					}else{
						$view->search($categorie, $menu, $categories, $detaille, $search, false, null, $annoncesByCategorie);
					}
				}
				
			}else{

				$cat = Categorie::where('categorie', '=', $categorie)->get();

				if (count($cat) > 0) {
				
					$categories = Categorie::all(); 
			
					if(is_null($result)){

						foreach ($cat as $key => $value) {
					
							$annoncesByCategorie = Annonce::where('id_categorie', '=', $value->id_categorie)->get();
						}
						
					}else {
						
						foreach ($cat as $key => $value) {
							
							$x = "id_categorie = $value->id_categorie and ";
						}

						$result = $x . $result;

						$annoncesByCategorie = Annonce::whereRaw($result)->get();
					}

					if($logged){
						$view->search($categorie, $menu, $categories, $detaille, $search, $logged, $mail, $annoncesByCategorie);
					}else{
						$view->search($categorie, $menu, $categories, $detaille, $search, false, null, $annoncesByCategorie);
					}

				}else {

				}	
			}

			$view->display();

		}else {

			DBConnection::makeConection();
			$view = new AnnonceView();
			$categories = Categorie::all();

			$view->search($categorie, $menu, $categories, $detaille, $search, $logged, $mail);
			$view->display();
		}

	}

	public function update($id, $menu, $put, $annuler){

		DBConnection::makeConection();
		$annonce = Annonce::with('categorie')->where('id_annonce', '=', $id)->get();
		$categories = Categorie::all();
		$view = new AnnonceView();

		$tok = $this->gererToken();

		$view->updateAnnonce($menu, $annonce, $put, $categories, $annuler, $tok);
		$view->display();
	}
	

	private function gererToken(){

		session_start();
		$token = hash("sha256", uniqid());
		$_SESSION['token'] = $token;

		return $token;
	}

	public function sauvegarder($id, $nomAnonce, $tarAnnonce, $villeAnnonce, $codePostal, $typeCat, $description) {

		$nomAnonce = strip_tags($nomAnonce);
		$villeAnnonce = strip_tags($villeAnnonce);
		$description = strip_tags($description);
		$tarAnnonce = strip_tags($tarAnnonce);
		$codePostal = strip_tags($codePostal);
		$id = strip_tags($id);
		$typeCat = strip_tags($typeCat);

		$nomAnonce = (empty($nomAnonce) or is_numeric($nomAnonce)) ? false : $nomAnonce;
		$villeAnnonce = (empty($villeAnnonce) or is_numeric($villeAnnonce)) ? false : $villeAnnonce;
		$description = (empty($description) or is_numeric($description)) ? false : $description;

		$tarAnnonce = (empty($tarAnnonce) or !is_numeric($tarAnnonce)) ? false : $tarAnnonce;
		$codePostal = (empty($codePostal) or !is_numeric($codePostal)) ? false : $codePostal;
		$id = (empty($id) or !is_numeric($id)) ? false : $id;
		$typeCat = (empty($typeCat) or !is_numeric($typeCat)) ? false : $typeCat; 

		if (!$nomAnonce or !$villeAnnonce or !$description or !$tarAnnonce or !$codePostal or !$id or !$typeCat) {
			
			$faite = false;

		}else {

			DBConnection::makeConection();
			date_default_timezone_set('Europe/Paris');

			$annonce = Annonce::find($id);

			$annonce->titre = $nomAnonce;
			$annonce->descr = $description;
			$annonce->prix = $tarAnnonce;
			$annonce->ville = $villeAnnonce;
			$annonce->cp = $codePostal;
			$annonce->id_categorie = $typeCat;
			$annonce->updated_at = date('Y-m-d H:i:s');

			$annonce->save();

			$faite = true;

		}	

		return $faite;
	}


	public function accueil($menu, $uri, $logged = false, $mail = null){

		DBConnection::makeConection();

		$slide = Annonce::all();

		$top = Annonce::with('categorie', 'user')->orderBy('created_at', 'desc')->take(9)->get();
		
		if($logged){
			$view = new AccueilView($menu, $slide, $top, $uri, $logged, $mail);
		}else{
			$view = new AccueilView($menu, $slide, $top, $uri);
		}

		$view->display();

	}

}


?>
<?php

class ControlleurLogin {

	protected $listValeur = array(
							"nom_user" => "Le champ Nom n'est pas valide", 
							"prenom_user" => "Champ Prenom n'est pas valide", 
							"tel_user" => "Le champ Tel doit être conformé d'une longuer de 10",
							"adress_user" => "Le champ Adresse n'est pas valide",
							"category" => "Champ Categorie",
							"mail_user" => "Le champ Email n'est pas valide",
							"password_user" => "Votre Password doit être d'une longuer au moins de 8 caractères");

	public function __construct() { }

	public function signupPro($menu, $url_post, $errors = null){

		DBConnection::makeConection();
		$categories = Categorie::all();

		$token = Securite::gererToken();
		
		if(is_null($errors)){
			$view = new SignupView($menu, $categories, $url_post, $token);
		}else {
			$view = new SignupView($menu, $categories, $url_post, $token, $errors);
		}

		$view->display();
	}

	public function makeUser(Array $params){


		if (is_array($params)) {
			
			foreach ($params as $key => $value) { $params[$key] = strip_tags($value); }

			foreach ($params as $key => $value) {
				
				if(strcmp($key, "tel_user") == 0)
					$params[$key] = (empty($value) || !is_numeric($value)) ? null : $value;
				

				if(strcmp($key, "password_user") == 0)
					$params[$key] = (empty($value) || is_numeric($value) || (strlen($value) < 8) ) ? null : $value;
				

				if(strcmp($key, "mail_user") == 0)
					$params[$key] = (empty($value) || is_numeric($value) || !(Securite::validerMail($value)) ) ? null : $value;
				

				if(strcmp($key, "category") == 0)
					$params[$key] = (empty($value) || !is_numeric($value)) ? null : $value;

				
				if( (strcmp($key, "nom_user") == 0) || (strcmp($key, "prenom_user") == 0) || (strcmp($key, "adress_user") == 0) )
					$params[$key] = (empty($value) || is_numeric($value)) ? null : $value;

			}

			
			if(in_array(null, $params, true)) {

				foreach ($this->listValeur as $key => $value) {
				
					if (is_null($params[$key])) {
						
						$errors['errors'][] = $value;
					}
				}

				$i = 0;
				foreach ($params as $key => $value) { 

					if( (strcmp($key, "password_user") != 0) && (strcmp($key, "token") != 0) && (strcmp($key, "category") != 0) ) {
						
						$errors["params"][$i]['valeur'] = $value;
						$errors["params"][$i]['id'] = $key;
						$errors["params"][$i]['titre'] = strtok($key, "_");
					}

					$i++;
				}

				return $errors;

			}else{

				DBConnection::makeConection();
				$user = new User;

				$sale = uniqid();
				$pwd = $params['password_user'] . $sale;
				$pwd = hash("sha256", $pwd);

				$user->email = $params['mail_user'];
				$user->nom = $params['nom_user'];
				$user->prenom = $params['prenom_user'];
				$user->telephone = $params['tel_user'];
				$user->adresse = $params['adress_user'];
				$user->cat_prefere = $params['category'];
				$user->pwd_usr = $pwd;
				$user->id_role = 2;
				$user->sale = $sale;

				if($user->save()){
					echo "ingresado. <br />";
					$user->email = $params['mail_user'];
					$this->createSession($user);

					return true;
				}


			}
			
		}
		
	}

	private function createSession(User $user){

		session_start();
		
		$ip = $_SERVER['REMOTE_ADDR'];
		$values = array(
					'username' => $user->prenom, 
					"userid" => $user->email, 
					"id_role" => $user->id_role, 
					"clien_ip" => $ip
				);

		$_SESSION['profil'] = $values;

	}

	public function checkMail($mail_user){

		DBConnection::makeConection();
		$user = User::where("email", "like", $mail_user)->get();

			if(count($user) > 0)
				return true;
			else 
				return false;
	}

	public function loginView($menu, $uri, $error = false){

		$token = Securite::gererToken();

		if($error){
			$view = new LoginView($menu, $uri, $token, $error);
		}
		else{
			$view = new LoginView($menu, $uri, $token);
		}

		$view->display();
	}

	public function loginUser(Array $params){

		if(is_array($params)){

			
			foreach ($params as $key => $value) {
				
				$params[$key] = strip_tags($value);
			}

			DBConnection::makeConection();

			$mail_user = $params['mail_usr'];
			$user = User::where("email", "like", $mail_user)->get();

			if(count($user) < 1){
				return false;
			}else {

				$sale = $user[0]->sale;
				$pwd = $params['password_usr'] . $sale;
				$pwd = hash("sha256", $pwd);
		
				$user = User::where("email", "like", $mail_user)->where("pwd_usr", "like", $pwd)->get();

				if(count($user) < 1){
					return false;
				}else {
					
					$this->createSession($user[0]);
					return true;	
				}
			}

			
		}
	}

	public function logoutUser(){

		Securite::logout();
	}
	
}

?>
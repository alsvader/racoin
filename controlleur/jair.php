<?php


class AnnonceControlleurJair
{
	public function __construct(){}

	
	public function insert($titre, $ville, $code_postal, $categorie, $tarif, $uploadedfile, $description, $teléphone, $mail, $key){

		$titre = strip_tags($titre);
		$ville = strip_tags($ville);
		$code_postal = strip_tags($code_postal);
		$categorie = strip_tags($categorie);
		$tarif = strip_tags($tarif);
		$description = strip_tags($description);
		$teléphone = strip_tags($teléphone);
		$mail = strip_tags($mail);
		$key = strip_tags($key);
		$uploadedfile;	
		$titre = (empty($titre) or is_numeric($titre)) ? false : $titre ;
		$ville = (empty($ville) or is_numeric($ville)) ? false : $ville ;
		$code_postal = (empty($code_postal) or !is_numeric($code_postal)) ? false : $code_postal;
		$categorie = (empty($categorie) or !is_numeric ($categorie)) ? false : $categorie ;
		$tarif = (empty($tarif) or !is_numeric($tarif)) ? false : $tarif ;
		$uploadedfile;
		$description = (empty($description) or is_numeric($description)) ? false : $description ;
		$teléphone = (empty($teléphone) or !is_numeric($teléphone)) ? false : $teléphone ;
		$mail = (!filter_var($mail, FILTER_VALIDATE_EMAIL)) ? false : $mail ;
		$key = (empty($key) or is_numeric($key)) ? false : $key ;


		if (!$titre or !$ville or !$code_postal or !$categorie or !$tarif or !$description or !$teléphone or !$mail or !$key) {
			
			$actionInsert = false;
			echo "error";

		}else{

			DBConnection::makeConection();
			date_default_timezone_set('Europe/Paris');

			$N_Annonce = new Annonce;
			$N_Annonce->titre = "$titre";
			$N_Annonce->descr = "$description";
			$N_Annonce->prix = "$tarif";
			$N_Annonce->ville = "$ville";
			$N_Annonce->cp = "$code_postal";
			$N_Annonce->id_categorie = "$categorie";
			$N_Annonce->id_user = "$mail";
			
			$sale = uniqid();
			$N_Annonce->sale=$sale;
			$key = $key.$sale;
			$key = hash("sha256", $key);
			$N_Annonce->passe = "$key";
			$N_Annonce->save();

			$actionInsert = true;
		}

		return $actionInsert;
	}

	public function CForm($menu, $insert){
		
		DBConnection::makeConection();
		$categorie= Categorie::all();

		$leTok = Securite::gererToken(); 

		$form = new FormView();
		$form->MontrerForm($menu, $categorie, $insert, $leTok);
		$form->display();
	}

	public function jsonAll($app){
	
		DBConnection::makeConection();
		$infos = Annonce::with("categorie")->select('titre as title', 'descr as description', 'prix as $','ville as location','cp as code_Postal','id_user as email','created_at as date','id_categorie' )->get();
		return $infos->toJson();
	}
}


?>
<?php

require 'vendor/autoload.php';


function  authentificationToken(){

	session_start();

	$app = \Slim\Slim::getInstance();

	if ( !($_SESSION['token'] == $app->request->put('token')) ) {
		
		$app->redirect('/');	
	}
}

function tokenPost(){

	session_start();

	$app = \Slim\Slim::getInstance();

	if ( !($_SESSION['token'] == $app->request->post('token')) ) {
		
		$app->redirect('/');	
	}
}

function checkSession(){

	session_start();

	if(isset($_SESSION['profil'])){

		return true;
	}
}

function nameSession(){

	session_start();
	return $_SESSION['profil']['userid'];
}

	
	$app = new \Slim\Slim(array(
			'debug' => true
		));

	
	$app->get('/', function () use ($app) {

		$controller = new ControlleurAaron();

		if(checkSession()){

			$menu = array(
                    array("href" => $app->urlFor("accueil"), "text" => "Accueil"),
                    array("href" => $app->urlFor("listAnnonces"), "text" => "Annonces"),
                    array("href" => $app->urlFor('nouvelleAnnonce'), "text" => "Ajouter une Annonce"),
                    array("href" => $app->urlFor('search'), "text" => "Search")
                );

			$logged = true;
			$mail = nameSession();
			$controller->accueil($menu, $app->urlFor('annonceId'), $logged, $mail);


		}else {
			$menu = array(
	                    array("href" => $app->urlFor("accueil"), "text" => "Accueil"),
	                    array("href" => $app->urlFor("listAnnonces"), "text" => "Annonces"),
	                    array("href" => $app->urlFor('nouvelleAnnonce'), "text" => "Ajouter une Annonce"),
	                    array("href" => $app->urlFor('search'), "text" => "Search"),
	                    array("href" => $app->urlFor('signup'), "text" => "Signup"),
	                    array("href" => $app->urlFor('login'), "text" => "Login")
	                );

			$controller->accueil($menu, $app->urlFor('annonceId'));

		}

	})->name('accueil');


	$app->get('/annonce/(:id)', function ($id) use ($app) {

		$controller = new ControlleurAaron();

		if(checkSession()){

			$menu = array(
                    array("href" => $app->urlFor("accueil"), "text" => "Accueil"),
                    array("href" => $app->urlFor("listAnnonces"), "text" => "Annonces"),
                    array("href" => $app->urlFor('nouvelleAnnonce'), "text" => "Ajouter une Annonce"),
                    array("href" => $app->urlFor('search'), "text" => "Search")
                );

			$logged = true;
			$mail = nameSession();
			$controller->afficheById($id, $menu, $app->urlFor('annonceId'), $app->urlFor("updateAnnonce"), $logged, $mail);

		}else {
			$menu = array(
	                    array("href" => $app->urlFor("accueil"), "text" => "Accueil"),
	                    array("href" => $app->urlFor("listAnnonces"), "text" => "Annonces"),
	                    array("href" => $app->urlFor('nouvelleAnnonce'), "text" => "Ajouter une Annonce"),
	                    array("href" => $app->urlFor('search'), "text" => "Search"),
	                    array("href" => $app->urlFor('signup'), "text" => "Signup"),
	                    array("href" => $app->urlFor('login'), "text" => "Login")
	                );
			$controller->afficheById($id, $menu, $app->urlFor('annonceId'), $app->urlFor("updateAnnonce"));
		}	

	})->name('annonceId');


	$app->get('/annonces/', function () use ($app) {
                
        $controller = new ControlleurAaron();

        if(checkSession()){

			$menu = array(
                    array("href" => $app->urlFor("accueil"), "text" => "Accueil"),
                    array("href" => $app->urlFor("listAnnonces"), "text" => "Annonces"),
                    array("href" => $app->urlFor('nouvelleAnnonce'), "text" => "Ajouter une Annonce"),
                    array("href" => $app->urlFor('search'), "text" => "Search")
                );

			$logged = true;
			$mail = nameSession();
			$controller->afficheAll($menu, $app->urlFor("annonceId"), $logged, $mail);

		}else {
			$menu = array(
	                    array("href" => $app->urlFor("accueil"), "text" => "Accueil"),
	                    array("href" => $app->urlFor("listAnnonces"), "text" => "Annonces"),
	                    array("href" => $app->urlFor('nouvelleAnnonce'), "text" => "Ajouter une Annonce"),
	                    array("href" => $app->urlFor('search'), "text" => "Search"),
	                    array("href" => $app->urlFor('signup'), "text" => "Signup"),
	                    array("href" => $app->urlFor('login'), "text" => "Login")
	                );

			$controller->afficheAll($menu, $app->urlFor("annonceId"));
		}

	})->name('listAnnonces');


	$app->get('/annonces/(:categorie)', function ($categorie) use ($app) {

		if(checkSession()){

			$menu = array(
                    array("href" => $app->urlFor("accueil"), "text" => "Accueil"),
                    array("href" => $app->urlFor("listAnnonces"), "text" => "Annonces"),
                    array("href" => $app->urlFor('nouvelleAnnonce'), "text" => "Ajouter une Annonce"),
                    array("href" => $app->urlFor('search'), "text" => "Search")
                );

		}else {
			$menu = array(
	                    array("href" => $app->urlFor("accueil"), "text" => "Accueil"),
	                    array("href" => $app->urlFor("listAnnonces"), "text" => "Annonces"),
	                    array("href" => $app->urlFor('nouvelleAnnonce'), "text" => "Ajouter une Annonce"),
	                    array("href" => $app->urlFor('search'), "text" => "Search"),
	                    array("href" => $app->urlFor('signup'), "text" => "Signup"),
	                    array("href" => $app->urlFor('login'), "text" => "Login")
	                );
		}

		$controller = new ControlleurAaron();
		$controller->search($menu, $app->urlFor('annonceId'), $app->urlFor('search'), $categorie);

	})->name('AnnonceByCategorie');	


	$app->get('/Search', function () use ($app) {

		$menu = array();

		if(checkSession()){

			$menu = array(
                    array("href" => $app->urlFor("accueil"), "text" => "Accueil"),
                    array("href" => $app->urlFor("listAnnonces"), "text" => "Annonces"),
                    array("href" => $app->urlFor('nouvelleAnnonce'), "text" => "Ajouter une Annonce"),
                    array("href" => $app->urlFor('search'), "text" => "Search")
                );
			
			$logged = true;
			$mail = nameSession();

		}else {
			$menu = array(
	                    array("href" => $app->urlFor("accueil"), "text" => "Accueil"),
	                    array("href" => $app->urlFor("listAnnonces"), "text" => "Annonces"),
	                    array("href" => $app->urlFor('nouvelleAnnonce'), "text" => "Ajouter une Annonce"),
	                    array("href" => $app->urlFor('search'), "text" => "Search"),
	                    array("href" => $app->urlFor('signup'), "text" => "Signup"),
	                    array("href" => $app->urlFor('login'), "text" => "Login")
	                );
			$logged = false;
			$mail = null;
		}

		if(empty($app->request()->get('typeCat'))){

			$controller = new ControlleurAaron();
			$controller->search($menu, $app->urlFor('annonceId'), $app->urlFor('search'), $logged, $mail);

		}else {
			
			$controller = new ControlleurAaron();
			$controller->search($menu, $app->urlFor('annonceId'), $app->urlFor('search'), $logged, $mail, $app->request->get('typeCat'), $app->request->get() );
		}

	})->name('search');


	$app->get('/annonce/update/(:id)', function($id) use ($app) {
		
		if(checkSession()){

			$menu = array(
                    array("href" => $app->urlFor("accueil"), "text" => "Accueil"),
                    array("href" => $app->urlFor("listAnnonces"), "text" => "Annonces"),
                    array("href" => $app->urlFor('nouvelleAnnonce'), "text" => "Ajouter une Annonce"),
                    array("href" => $app->urlFor('search'), "text" => "Search")
                );

		}else {
			$menu = array(
	                    array("href" => $app->urlFor("accueil"), "text" => "Accueil"),
	                    array("href" => $app->urlFor("listAnnonces"), "text" => "Annonces"),
	                    array("href" => $app->urlFor('nouvelleAnnonce'), "text" => "Ajouter une Annonce"),
	                    array("href" => $app->urlFor('search'), "text" => "Search"),
	                    array("href" => $app->urlFor('signup'), "text" => "Signup"),
	                    array("href" => $app->urlFor('login'), "text" => "Login")
	                );
		}

		$controller = new ControlleurAaron();
		$controller->update($id, $menu, $app->urlFor('updatePut'), $app->urlFor("annonceId"));

	})->name('updateAnnonce');


	$app->put('/annonce/update/(:id)', 'authentificationToken', function ($id) use ($app) {
		
		$nomAnonce = $app->request->put('nom');
		$tarAnnonce = $app->request->put('tarAnnonce');
		$villeAnnonce = $app->request->put('villeAnnonce');
		$codePostal = $app->request->put('codePostal');
		$typeCat = $app->request->put('typeCat');
		$description = $app->request->put('description');

		$controller = new ControlleurAaron();
		$retourne = $controller->sauvegarder($id, $nomAnonce, $tarAnnonce, $villeAnnonce, $codePostal, $typeCat, $description);

		if ($retourne) {
				
			$app->redirect("/annonce/$id");

		}else{

			$app->redirect("/annonce/update/$id");
		}


	})->name('updatePut');
	

	$app->get('/signup', function () use ($app) {

		if(checkSession()){
			$app->redirect("/");
		}else{

			$menu = array(
	                    array("href" => $app->urlFor("accueil"), "text" => "Accueil"),
	                    array("href" => $app->urlFor("listAnnonces"), "text" => "Annonces"),
	                    array("href" => $app->urlFor('nouvelleAnnonce'), "text" => "Ajouter une Annonce"),
	                    array("href" => $app->urlFor('search'), "text" => "Search"),
	                    array("href" => $app->urlFor('signup'), "text" => "Signup"),
	                    array("href" => $app->urlFor('login'), "text" => "Login")
	                );

			$controller = new ControlleurLogin();
			
			if(empty($app->request()->get())){
				$controller->signupPro($menu, $app->urlFor('signupPost'));
			}
			else{

				$tmp = $app->request()->get('errors');

				$tmp = stripslashes($tmp);
				$tmp = urldecode($tmp);
				$tmp = unserialize($tmp);
				
				$controller->signupPro($menu, $app->urlFor('signupPost'), $tmp);
			}
		}
		
	})->name('signup');


	$app->post('/signup', 'tokenPost', function () use ($app) {

		$controlleurLog = new ControlleurLogin();
		$result = $controlleurLog->makeUser($app->request()->post());

		if(is_array($result)){

			$compactada = serialize($result);
			$compactada = urlencode($compactada);

			$uri = $app->urlFor('signup') . '?errors=' . $compactada;

			$app->redirect($uri);

		}elseif($result){
			$app->redirect('/');
		}

	})->name('signupPost');


	$app->post('/checkMail', function () use ($app){

		$controller = new ControlleurLogin();

		$data = $app->request()->post('mail_user');

		$result = $controller->checkMail($data);

			if($result)
				echo "error";
			else
				echo "success";		

	})->name('checkMail');


	$app->get('/login', function () use ($app){

		if(checkSession()){
			$app->redirect("/");
		}else{
			
			$menu = array(
	                    array("href" => $app->urlFor("accueil"), "text" => "Accueil"),
	                    array("href" => $app->urlFor("listAnnonces"), "text" => "Annonces"),
	                    array("href" => $app->urlFor('nouvelleAnnonce'), "text" => "Ajouter une Annonce"),
	                    array("href" => $app->urlFor('search'), "text" => "Search"),
	                    array("href" => $app->urlFor('signup'), "text" => "Signup"),
	                    array("href" => $app->urlFor('login'), "text" => "Login")
	                );

			$controller = new ControlleurLogin();
			
			if(!empty($app->request()->get('error')) && strcmp($app->request()->get('error'), "error") == 0){
				$controller->loginView($menu, $app->urlFor('loginPost'), $error = true);
			}
			else{
				$controller->loginView($menu, $app->urlFor('loginPost'));
			}
		}

	})->name('login');
	

	$app->post('/login', "tokenPost", function () use($app){

		$controller = new ControlleurLogin();
		$reponse = $controller->loginUser($app->request()->post());

		if($reponse){
			$app->redirect('/');
		}else{
			$app->redirect('/login?error=error');
		}

	})->name('loginPost');


	$app->get('/logout', function () use($app){

		$controller = new ControlleurLogin();
		$controller->logoutUser();
		$app->redirect('/');
	});


	$app->get('/annonce/nouvelle/', function () use ($app) {

		$menu = array(
					array("href" => $app->urlFor("accueil"),"text"=>"Accueil"),
                    array("href" => $app->urlFor("listAnnonces"), "text" => "Annonces"),
                    array("href" => $app->urlFor('nouvelleAnnonce'), "text" => "Ajouter une Annonce"),
                    array("href" => $app->urlFor('search'), "text" => "Search"),
	                array("href" => $app->urlFor('signup'), "text" => "Signup"),
	                array("href" => $app->urlFor('login'), "text" => "Login")
			);

		$controller = new AnnonceControlleurJair();
		$controller->CForm($menu, $app->urlFor('nouvelleAnnonce'));

	})->name('nouvelleAnnonce');


	$app->post('/annonce/nouvelle/', 'tokenPost', function() use ($app){

		$titre = $app->request->post('titre');
		$ville = $app->request->post('ville');
		$code_postal = $app->request->post('code_postal');
		$categorie = $app->request->post('categorie');
		$tarif = $app->request->post('tarif');
		$uploadedfile = $app->request->post("photo");
		$description = $app->request->post('description');
		$teléphone = $app->request->post('teléphone');
		$mail = $app->request->post('mail');
		$key = $app->request->post('key');

		$controller = new AnnonceControlleurJair();
		$leretourne = $controller->insert($titre, $ville, $code_postal, $categorie, $tarif, $uploadedfile, $description, $teléphone, $mail, $key);
		
		if ($leretourne) {
			$app->redirect("/annonce/nouvelle/");
		}else{
			$app->redirect("/");
		}

	})->name('nouvelleAnnoncePut');


	/**********************************************
		URI's pour l'API REST
	************************************************/

	$app->get('/rest/annonces/', function() use($app){

		$app->response->headers->set('Content-Type', 'application/json');

		$controller = new ControllerRest();
		$annonces = $controller->jsonAll($app);
		$app->response->setStatus(202);
		echo $annonces; 

	});

	$app->Put('/rest/annoncesupdate/(:id)',function($id)use($app){     

    	$json = '{"nom":"annonce rest","tarAnnonce":"123","villeAnnonce":"nancy","codePostal":"123456","typeCat":"1","description":"prueba rest"}';

    	$restAnnonce = new ControlleurIrving();
    	$restAnnonce->Put($id, $app , $json); 	
    });

	$app->run();
		

?>